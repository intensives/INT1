import pytest
from faker import Faker

fake = Faker()

@pytest.fixture(scope="module", autouse=True)
def db_setup(db_connection):
    cursor = db_connection.cursor()
    data = [(fake.text(max_nb_chars=10),) for _ in range(10000)]
    cursor.executemany("INSERT INTO perf_table (str) VALUES (%s);", data)
    db_connection.commit()
    yield
    cursor.execute("DELETE FROM perf_table")
    db_connection.commit()
    cursor.close()


@pytest.fixture()
def create_index(db_connection):
    cursor = db_connection.cursor()
    cursor.execute('CREATE INDEX idx_str ON perf_table (str);')
    yield
    cursor.execute('DROP INDEX idx_str ON perf_table;')
    cursor.close()

def execute_query(cursor, query):
    cursor.execute(query)
    return cursor.fetchall()

test_queries = [
    "SELECT * FROM perf_table WHERE str LIKE 's%';",
    "SELECT * FROM perf_table WHERE str LIKE 'a%t';",
    "SELECT * FROM perf_table WHERE str LIKE '%s';",
    "SELECT * FROM perf_table WHERE str LIKE '_s%';",
    "SELECT * FROM perf_table WHERE str LIKE 's%' OR str LIKE 'a%';"
]

# Тест без использованием индекса
@pytest.mark.parametrize("query", test_queries, ids=["'s%'", "'a%t'", "'%s'", "'_s%'", "'s% OR a%'"])
@pytest.mark.benchmark
def test_select_without_index(benchmark, db_connection, query):
    benchmark(execute_query, db_connection.cursor(), query)

# Тест с использованием индекса
@pytest.mark.parametrize("query", test_queries, ids=["'s%'", "'a%t'", "'%s'", "'_s%'", "'s% OR a%'"])
@pytest.mark.benchmark
def test_select_with_index(benchmark,create_index, db_connection, query):
    benchmark(execute_query, db_connection.cursor(), query)
