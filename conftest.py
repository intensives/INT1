import pytest
import mysql.connector

@pytest.fixture(scope="session")
def db_connection():
    conn = mysql.connector.connect(
        host="localhost",
        user=" ",
        password=" ",
        database = "test_db"
    )
    yield conn
    conn.close()

