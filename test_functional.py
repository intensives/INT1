import pytest

@pytest.fixture()
def db_setup(db_connection):
    cursor = db_connection.cursor()
    cursor.executemany('INSERT INTO test_table (id, str) VALUES (%s, %s)', 
    [
        (1, 'abcd'),
        (2, 'abc'),
        (3, '50% off'),
        (4, 'test_string'),
        (5, ''),
        (6, None)
    ])
    yield
    cursor.execute("DELETE FROM test_table")
    db_connection.commit()
    cursor.close()

@pytest.fixture()
def create_index(db_connection):
    cursor = db_connection.cursor()
    cursor.execute('CREATE INDEX idx_str ON test_table (str);')
    yield
    cursor.execute('DROP INDEX idx_str ON test_table;')
    cursor.close()

def execute_query(cursor, query):
    cursor.execute(query)
    return cursor.fetchall()

# Тестовые запросы
test_queries = [
    "SELECT * FROM test_table WHERE str LIKE 'abcd';",
    "SELECT * FROM test_table WHERE str LIKE 'abc%';",
    "SELECT * FROM test_table WHERE str LIKE '%bc';",
    "SELECT * FROM test_table WHERE str LIKE '%bc%';",
    "SELECT * FROM test_table WHERE str LIKE 'a%c_';",
    "SELECT * FROM test_table WHERE str LIKE '50\\% off';",
    "SELECT * FROM test_table WHERE str LIKE '';",
    "SELECT * FROM test_table WHERE str IS NULL;",
    "SELECT * FROM test_table WHERE str LIKE 'tes%' AND str LIKE '%ing';"
]
# Ожидаемые результаты
exp_res = [
    [(1, 'abcd')],
    [(1, 'abcd'), (2, 'abc')],
    [(2, 'abc')],
    [(1, 'abcd'), (2, 'abc')],
    [(1, 'abcd')],
    [(3, '50% off')],
    [(5, '')],
    [(6, None)],
    [(4, 'test_string')],
]

# Тестовая функция для проверки результатов запросов без индекса
@pytest.mark.parametrize("query, expected", zip(test_queries, exp_res))
def test_queries_without_index(db_connection, db_setup, query, expected):
    cursor = db_connection.cursor()
    result = execute_query(cursor, query)
    cursor.close()
    assert result == expected
    

# Тестовая функция для проверки результатов запросов с индексом
@pytest.mark.parametrize("query, expected", zip(test_queries, exp_res))
def test_queries_with_index(db_connection, db_setup, create_index, query, expected):
    cursor = db_connection.cursor()
    result = sorted(execute_query(cursor, query)) #Нужно сортировать, с индексом порядок другой
    cursor.close()
    assert result == expected

###
# Изначально написал тест представленный ниже, который просто сравнивает результаты запросов без индекса и с индексом,
# как и требуется в задании. Но затем решил поработать с параметризированными тестами, написал два теста, представленные выше,
# которые сравнивают результаты с ожидаемыми, тем самым проверяется и корректность самих запросов, и само требование задания.
# Данный подход мне показался более правильным, но применим только, когда можно посчитать ожидаемые результаты.
###
# def test_functional_index(db_connection, db_setup):
#     cursor = db_connection.cursor()
#     # Сохраняем результаты выполнения запросов без индекса
#     results_without_index = {}
#     for query in test_queries:
#         results_without_index[query] = execute_query(cursor,query)
    
#     # Создаем индекс
#     cursor.execute('CREATE INDEX idx_str ON test_table (str);')

#     # Сохраняем результаты выполнения запросов c индексом
#     results_with_index = {}
#     for query in test_queries:
#         results_with_index[query] = sorted(execute_query(cursor,query))

#     # Удаляем индекс
#     cursor.execute('DROP INDEX idx_str ON test_table;')
    
#     # Сравниваем результаты
#     for query in test_queries:
#         assert results_without_index[query] == results_with_index[query]
    
    